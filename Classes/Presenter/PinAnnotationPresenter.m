//
//  PinAnnotationPresenter.m
//  codesample
//
//  Created by Sergey Emelyanov on 12/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import "PinAnnotationPresenter.h"

@implementation PinAnnotationPresenter

- (void)fireSelection
{
    [self.interactor fireSelection:self.company];
}

- (void)updateState:(NSObject*)value
{
    UIColor *resultColor = [UIColor greenColor];
    if (self.company == (NSIndexPath*)value)
    {
        resultColor = [UIColor blueColor];
    }
    [self.view setNewColor:resultColor];
}

@end
