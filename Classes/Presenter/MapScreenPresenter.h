//
//  MapScreenPresenter.h
//  codesample
//
//  Created by Sergey Emelyanov on 05/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MapScreenView.h"
#import "MapScreenInteractorIO.h"
#import "PinAnnotationInteractorIO.h"
#import "CalloutMapAnnotation.h"
#import "PinAnnotationInteractor.h"

@interface MapScreenPresenter : NSObject <MapScreenInteractorOutput, PinAnnotationInteractorOutput>

@property (nonatomic, weak)     id<MapScreenView>            view;
@property (nonatomic, strong)   id<MapScreenInteractorInput> interactor;
@property (nonatomic, strong)   PinAnnotationInteractor      *pinInteractor;
@property (nonatomic, strong)   NSArray                      *companyList;
@property (nonatomic, strong)   NSMutableArray               *eventsList;
@property (nonatomic, assign)   NSIndexPath                  *selectedCompany;
@property (nonatomic, assign)   BOOL                         isCompanySelected;

- (void)requestData;

- (void)didSelectEventAtIndex:(NSIndexPath*)value isCompany:(BOOL)flag;

- (NSInteger)getEventsCount;
- (UITableViewCell*)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

- (NSArray*)getAnnotations;
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation;

@end
