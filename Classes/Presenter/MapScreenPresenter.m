//
//  MapScreenPresenter.m
//  codesample
//
//  Created by Sergey Emelyanov on 05/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import "MapScreenPresenter.h"
#import "EventClass.h"
#import "CompanyClass.h"
#import "PinAnnotationViewController.h"
#import "PinAnnotationPresenter.h"

@implementation MapScreenPresenter

- (void)requestData
{
    [self.interactor requestData];
}

#pragma mark - Interactor output

- (void)updateCompanyList:(NSArray*)value
{
    [self setCompanyList:value];
    [self setSelectedCompany:[NSIndexPath indexPathForRow:0 inSection:0]];
    [self setIsCompanySelected:NO];
    [self setEventsList:[[NSMutableArray alloc] init]];
    
    for(CompanyClass *company in self.companyList)
    {
        for(EventClass *event in company.events)
        {
            [self.eventsList addObject:event];
        }
    }
    [self.view refreshCompany];
    [self.view refreshEvents];
}

- (void) sendError:(NSError *)error
{
    [self.view showError:error];
}

- (void)updateState:(NSObject*)value
{
    [self updateSelections:(NSIndexPath*)value
                 isCompany:YES];
}

- (void) updateSelections:(NSIndexPath*)value isCompany:(BOOL)flag
{
    [self setSelectedCompany:value];
    [self setIsCompanySelected:flag];
    [self.view refreshEvents];
}

#pragma mark - TableView/MKMap Delegate

- (void)didSelectEventAtIndex:(NSIndexPath*)value isCompany:(BOOL)flag
{
    for(CompanyClass *company in self.companyList)
    {
        if (company.organizationId == [[self.eventsList objectAtIndex:value.row] organizationId])
        {
            [self.pinInteractor fireSelection:[NSIndexPath indexPathForRow:0
                                                                 inSection:company.organizationId]];
            break;
        }
    }
    [self updateSelections:value isCompany:flag];
}

#pragma mark - TableViewDelegate

- (NSInteger)getEventsCount
{
    NSInteger summary = 0;
    for(CompanyClass *item in self.companyList)
    {
        summary += [item.events count];
    }
    return summary;
}

- (UITableViewCell*)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:TableViewCellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell setBackgroundColor:[UIColor whiteColor]];
    
    EventClass *event = [self.eventsList objectAtIndex:indexPath.row];
    [cell.textLabel setText:event.title];
    [cell.detailTextLabel setText:event.organizationTitle];
    if ((self.isCompanySelected && (self.selectedCompany.section == event.organizationId)) ||
        (!self.isCompanySelected && self.selectedCompany.row == indexPath.row))
    {
        [cell setBackgroundColor:[UIColor blueColor]];
        [cell setSelected:YES];
    }
    return cell;
}

#pragma mark - MKMapView Delegate

- (NSArray*)getAnnotations {
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    for(CompanyClass *company in self.companyList)
    {
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        CalloutMapAnnotation *coord = [[CalloutMapAnnotation alloc] initWithLatitude:company.latitude
                                                                        andLongitude:company.longitude];
        point.coordinate = coord.coordinate;
        [point setTitle:company.title];
        [annotations addObject:point];
    }
    return [annotations copy];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *annotaionIdentifier=@"annotationIdentifier";
    PinAnnotationViewController *aView=(PinAnnotationViewController*)[mapView dequeueReusableAnnotationViewWithIdentifier:annotaionIdentifier ];
    
    if (aView==nil)
    {
        aView=[[PinAnnotationViewController alloc] initWithAnnotation:annotation
                                                      reuseIdentifier:annotaionIdentifier];
        aView.calloutOffset = CGPointMake(-5, 5);
        if (!self.pinInteractor)
        {
            [self setPinInteractor:[[PinAnnotationInteractor alloc] init]];
            [self.pinInteractor setOutput: self];
        }
        PinAnnotationPresenter* presenter = [[PinAnnotationPresenter alloc] init];
            
        aView.presenter = presenter;
        presenter.view = aView;
            
        presenter.interactor = self.pinInteractor;
        [self.pinInteractor setOutput: aView.presenter];
    }
    aView.pinTintColor = [UIColor greenColor];
    
    for(CompanyClass *company in self.companyList)
    {
        if (annotation.coordinate.latitude == company.latitude && annotation.coordinate.longitude == company.longitude)
        {
            [aView.presenter setCompany:[NSIndexPath indexPathForRow:0
                                                           inSection:company.organizationId]];
            if ((self.isCompanySelected && self.selectedCompany.section == company.organizationId) ||
                (!self.isCompanySelected && [(EventClass*)[self.eventsList objectAtIndex:self.selectedCompany.row] organizationId] == company.organizationId))
            {
                aView.pinTintColor = [UIColor blueColor];
            }
            break;
        }
    }
    
    return aView;
}

@end
