//
//  PinAnnotationPresenter.h
//  codesample
//
//  Created by Sergey Emelyanov on 12/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PinAnnotationView.h"
#import "PinAnnotationInteractorIO.h"
#import "CompanyClass.h"

@interface PinAnnotationPresenter : NSObject <PinAnnotationInteractorOutput>

@property (nonatomic, weak)     id<PinAnnotationView>               view;
@property (nonatomic, strong)   id<PinAnnotationInteractorInput>    interactor;
@property (nonatomic, assign)   NSIndexPath                         *company;

- (void)fireSelection;

@end
