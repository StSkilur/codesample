//
//  MapScreenViewController.h
//  codesample
//
//  Created by Sergey Emelyanov on 05/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapScreenView.h"

@class MapScreenPresenter;

@interface MapScreenViewController : UIViewController <MapScreenView, UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate>

@property (nonatomic, weak)   IBOutlet    UITableView           *tableView;
@property (nonatomic, weak)   IBOutlet    MKMapView             *mapView;
@property (nonatomic, strong)             MapScreenPresenter    *presenter;

@end

