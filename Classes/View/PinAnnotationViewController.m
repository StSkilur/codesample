//
//  PinAnnotationViewController.m
//  codesample
//
//  Created by Sergey Emelyanov on 13/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import "PinAnnotationViewController.h"
#import "PinAnnotationPresenter.h"

@implementation PinAnnotationViewController

- (void)setNewColor:(UIColor*)pinTintColor
{
    self.pinTintColor = pinTintColor;
    [self setNeedsDisplay];
    [self tintColorDidChange];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches
           withEvent:(UIEvent *)event
{
    [self.presenter fireSelection];
    [super touchesEnded:(NSSet<UITouch *> *)touches
                      withEvent:(UIEvent *)event];
}

@end
