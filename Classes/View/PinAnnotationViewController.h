//
//  PinAnnotationViewController.h
//  codesample
//
//  Created by Sergey Emelyanov on 13/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "PinAnnotationView.h"

@class PinAnnotationPresenter;

@interface PinAnnotationViewController : MKPinAnnotationView <PinAnnotationView>

@property (nonatomic, strong)   PinAnnotationPresenter  *presenter;

@end
