//
//  MapScreenViewController.m
//  codesample
//
//  Created by Sergey Emelyanov on 05/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import "MapScreenViewController.h"
#import "MapScreenPresenter.h"

NSString * const TableViewCellIdentifier = @"CellIdentifier";

@interface MapScreenViewController ()

@end

@implementation MapScreenViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.presenter requestData];
}

#pragma mark - Active view

- (void)refreshEvents
{
    [self.tableView reloadData];
}

- (void)refreshCompany
{
    [self removeAllCompany];
    [self.mapView addAnnotations:[self.presenter getAnnotations]];
}

- (void)removeAllCompany
{
    [self.mapView removeAnnotations:[self.mapView annotations]];
}

#pragma mark - Table View

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.presenter didSelectEventAtIndex:[NSIndexPath indexPathForRow:indexPath.row inSection:0]
                                isCompany:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.presenter getEventsCount];
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.presenter tableView:(UITableView *)theTableView
               cellForRowAtIndexPath:(NSIndexPath *)indexPath];
}

#pragma mark - MKMapView Delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    return [self.presenter mapView:mapView
                 viewForAnnotation:annotation];
}

- (void)showError:(NSError*)error
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"We have not a good news..."
                                 message:error.localizedDescription
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                                actionWithTitle:@"ReTry"
                                style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [self.presenter requestData];
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No, thanks"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    [alert addAction:okButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
