//
//  PinAnnotationView.h
//  codesample
//
//  Created by Sergey Emelyanov on 13/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PinAnnotationView <NSObject>

- (void)setNewColor:(UIColor*)pinTintColor;

@end
