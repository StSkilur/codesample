//
//  MapScreenView.h
//  codesample
//
//  Created by Sergey Emelyanov on 05/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const TableViewCellIdentifier;

@protocol MapScreenView <NSObject>

- (void)refreshCompany;
- (void)refreshEvents;
- (void)showError:(NSError*)error;

@end
