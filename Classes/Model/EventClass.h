//
//  VisitClass.h
//  codesample
//
//  Created by Sergey Emelyanov on 04/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventClass : NSObject

@property (nonatomic, assign) NSInteger organizationId;
@property (nonatomic, strong) NSString   *title;
@property (nonatomic, strong) NSString   *organizationTitle;

+ (EventClass *) createWithAttributes:(NSDictionary *)attributes;

@end
