//
//  CompanyClass.m
//  codesample
//
//  Created by Sergey Emelyanov on 04/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import "CompanyClass.h"

@implementation CompanyClass

+ (CompanyClass *) createWithAttributes:(NSDictionary *)attributes
{
    CompanyClass *me = [[CompanyClass alloc] init];
    [me setOrganizationId:[[attributes objectForKey:@"organizationId"] integerValue]];
    [me setTitle:[attributes objectForKey:@"title"]];
    [me setLatitude:(CGFloat)[[attributes objectForKey:@"latitude"] doubleValue]];
    [me setLongitude:(CGFloat)[[attributes objectForKey:@"longitude"] doubleValue]];
    [me setEvents:[[NSMutableArray alloc] init]];
    return me;
}

@end
