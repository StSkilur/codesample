//
//  CompanyClass.h
//  codesample
//
//  Created by Sergey Emelyanov on 04/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyClass : NSObject

+ (CompanyClass *) createWithAttributes:(NSDictionary *)attributes;

@property (nonatomic, assign) NSInteger      organizationId;
@property (nonatomic, strong) NSString       *title;
@property (nonatomic, assign) CGFloat        latitude;
@property (nonatomic, assign) CGFloat        longitude;
@property (nonatomic, strong) NSMutableArray *events;

@end
