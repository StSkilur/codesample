//
//  VisitClass.m
//  codesample
//
//  Created by Sergey Emelyanov on 04/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import "EventClass.h"

@implementation EventClass

+ (EventClass *) createWithAttributes:(NSDictionary *)attributes
{
    EventClass *me = [[EventClass alloc] init];
    [me setOrganizationId:[[attributes objectForKey:@"organizationId"] integerValue]];
    [me setTitle:[attributes objectForKey:@"title"]];
    return me;
}

@end
