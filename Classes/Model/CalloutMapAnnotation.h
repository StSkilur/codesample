//
//  CalloutMapAnnotation.h
//  codesample
//
//  Created by Sergey Emelyanov on 04/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface CalloutMapAnnotation : NSObject<MKAnnotation>
{
}

- (CalloutMapAnnotation *) initWithLatitude:(CLLocationDegrees)latitude andLongitude:(CLLocationDegrees)longitude;

@property (nonatomic) CLLocationDegrees latitude;
@property (nonatomic) CLLocationDegrees longitude;

@end
