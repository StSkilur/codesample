//
//  GetEventsCommand.h
//  codesample
//
//  Created by Даша on 18/03/2017.
//  Copyright © 2017 sbt. All rights reserved.
//

#import "LoadData.h"

@interface GetEventsCommand : LoadData

@property (nonatomic, copy) APISuccess success;
@property (nonatomic, copy) APIFailure failure;

+ (GetEventsCommand *) createWithsuccess:(APISuccess)theSuccess failure:(APIFailure)theFailure;
- (void) execute;

@end
