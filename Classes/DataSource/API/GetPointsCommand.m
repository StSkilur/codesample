//
//  GetPointsCommand.m
//  codesample
//
//  Created by Даша on 18/03/2017.
//  Copyright © 2017 sbt. All rights reserved.
//

#import "GetPointsCommand.h"

@implementation GetPointsCommand

+ (GetPointsCommand *) createWithsuccess:(APISuccess)theSuccess failure:(APIFailure)theFailure
{
    GetPointsCommand *command = [[GetPointsCommand alloc] init];
    [command setSuccess:theSuccess];
    [command setFailure:theFailure];
    return command;
}

#pragma mark - LoadDataCommand

- (void) execute
{
    [self getJsonResponse:POINTSURL success:^(NSArray *responseArray)
     {
         self.success(responseArray);
     } failure:^(NSError *error)
     {
         self.failure(error);
     }];
}

@end
