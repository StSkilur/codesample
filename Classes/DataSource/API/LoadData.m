//
//  LoadData.m
//  codesample
//
//  Created by Sergey Emelyanov on 05/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import "LoadData.h"

@implementation LoadData

- (void)getJsonResponse:(NSString *)urlStr success:(APISuccess)success failure:(APIFailure)failure
{
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                            {
                                                if (error)
                                                    failure(error);
                                                else
                                                {
                                                    NSError *e = nil;
                                                    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&e];
                                                    
                                                    if (!jsonArray) {
                                                        failure(e);
                                                    } else {
                                                        success(jsonArray);
                                                    }
                                                }
                                            }];
    [dataTask resume];
}

@end
