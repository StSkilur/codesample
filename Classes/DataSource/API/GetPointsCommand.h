//
//  GetPointsCommand.h
//  codesample
//
//  Created by Даша on 18/03/2017.
//  Copyright © 2017 sbt. All rights reserved.
//

#import "LoadData.h"

@interface GetPointsCommand : LoadData

@property (nonatomic, copy) APISuccess success;
@property (nonatomic, copy) APIFailure failure;

+ (GetPointsCommand *) createWithsuccess:(APISuccess)theSuccess failure:(APIFailure)theFailure;
- (void) execute;

@end
