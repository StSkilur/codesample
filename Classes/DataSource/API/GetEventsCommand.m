//
//  GetEventsCommand.m
//  codesample
//
//  Created by Даша on 18/03/2017.
//  Copyright © 2017 sbt. All rights reserved.
//

#import "GetEventsCommand.h"

@implementation GetEventsCommand

+ (GetEventsCommand *) createWithsuccess:(APISuccess)theSuccess failure:(APIFailure)theFailure
{
    GetEventsCommand *command = [[GetEventsCommand alloc] init];
    [command setSuccess:theSuccess];
    [command setFailure:theFailure];
    return command;
}

#pragma mark - LoadDataCommand

- (void) execute
{
    [self getJsonResponse:EVENTSURL success:^(NSArray *responseArray)
     {
         self.success(responseArray);
     } failure:^(NSError *error)
     {
         self.failure(error);
     }];
}

@end
