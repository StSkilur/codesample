//
//  LoadData.h
//  codesample
//
//  Created by Sergey Emelyanov on 05/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"
#import "ResourcesUrls.h"

@interface LoadData : NSObject

- (void)getJsonResponse:(NSString *)urlStr success:(APISuccess)success failure:(APIFailure)failure;

@end
