//
//  Data.m
//  codesample
//
//  Created by Sergey Emelyanov on 05/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import "Data.h"

@implementation Data

- (void) getData:(APISuccess)success failure:(void(^)(NSError* error))failure
{
    GetEventsCommand *getEvents = [GetEventsCommand createWithsuccess:^(NSArray *responseArray)
                                   {
                                       for(NSDictionary *item in responseArray)
                                       {
                                           EventClass *tempEvent = [EventClass createWithAttributes:item];
                                           for(CompanyClass *item in self.company)
                                           {
                                               if(tempEvent.organizationId == item.organizationId)
                                               {
                                                   [tempEvent setOrganizationTitle:item.title];
                                                   [item.events addObject:tempEvent];
                                               }
                                           }
                                       }
                                       success([self.company copy]);
                                   } failure:^(NSError *error)
                                   {
                                       failure(error);
                                       // error handling here ...
                                   }];
    
    GetPointsCommand *getPoints = [GetPointsCommand createWithsuccess:^(NSArray *responseArray)
                                   {
                                       NSMutableArray *companyArray = [[NSMutableArray alloc] init];
                                       for(NSDictionary *item in responseArray)
                                       {
                                           [companyArray addObject:[CompanyClass createWithAttributes:item]];
                                       }
                                       [self setCompany:companyArray];
                                       
                                       [getEvents execute];
                                   } failure:^(NSError *error)
                                   {
                                       failure(error);
                                       // error handling here ...
                                   }];
    [getPoints execute];
}

@end
