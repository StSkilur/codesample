//
//  Data.h
//  codesample
//
//  Created by Sergey Emelyanov on 05/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"
#import "GetEventsCommand.h"
#import "GetPointsCommand.h"
#import "EventClass.h"
#import "CompanyClass.h"

@interface Data : NSObject

@property (nonatomic, strong)   NSArray *company;

- (void) getData:(APISuccess)success failure:(APIFailure)failure;

@end
