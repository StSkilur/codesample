//
//  MapScreenInteractor.m
//  codesample
//
//  Created by Sergey Emelyanov on 05/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import "MapScreenInteractor.h"
#import "Data.h"
#import "PinAnnotationInteractor.h"
#import "PinAnnotationPresenter.h"
#import "PinAnnotationViewController.h"

@interface MapScreenInteractor()
@property (nonatomic, assign)   NSInteger  activeID;
@end

@implementation MapScreenInteractor

- (void)requestData
{
    Data *api = [[Data alloc] init];
    [api getData:^(NSArray *response)
     {
         [self performSelectorOnMainThread:@selector(setCompanyList:)
                                withObject:response
                             waitUntilDone:YES];
         
         [self performSelectorOnMainThread:@selector(sendData)
                                withObject:nil
                             waitUntilDone:NO];
     } failure:^(NSError *error)
     {
         [self performSelectorOnMainThread:@selector(sendError:)
                                withObject:error
                             waitUntilDone:NO];
         // error handling here ...
     }];
}

- (void)sendData
{
    [self.output updateCompanyList:self.companyList];
}

- (void)sendError:(NSError*)error
{
    [self.output sendError:error];
}

@end
