//
//  MapScreenInteractor.h
//  codesample
//
//  Created by Sergey Emelyanov on 05/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MapScreenInteractorIO.h"

@interface MapScreenInteractor : NSObject <MapScreenInteractorInput>

@property (nonatomic, weak) id<MapScreenInteractorOutput>   output;
@property(nonatomic,strong) NSArray                         *companyList;

@end
