//
//  PinAnnotationInteractorIO.h
//  codesample
//
//  Created by Sergey Emelyanov on 12/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//


#import <Foundation/Foundation.h>


@protocol PinAnnotationInteractorInput <NSObject>

- (void)fireSelection:(NSObject*)value;

@end

@protocol PinAnnotationInteractorOutput <NSObject>

- (void)updateState:(NSObject*)value;

@end
