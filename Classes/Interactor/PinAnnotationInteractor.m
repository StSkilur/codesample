//
//  PinAnnotationInteractor.m
//  codesample
//
//  Created by Sergey Emelyanov on 12/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import "PinAnnotationInteractor.h"
#import "PinAnnotationPresenter.h"

@interface PinAnnotationInteractor()
@property (nonatomic, assign)   NSInteger  activeID;
@end

@implementation PinAnnotationInteractor

- (void)setOutput:(id<NSObject>)output
{
    if ([self.outputList count] == 0)
    {
        [self setOutputList:[[NSMutableArray alloc] init]];
    }
    [self.outputList addObject:output];
}

- (void)fireSelection:(NSObject*)value
{
    [self updateState:value withMap:NO];
}

- (void)fireFromOutSelection:(NSObject*)value
{
    [self updateState:value withMap:YES];
}

- (void)updateState:(NSObject*)value withMap:(BOOL)isMap
{
    for(id outputItem in self.outputList)
    {
        __strong id strongOutput = outputItem;
        if (strongOutput) {
            if (isMap && ![strongOutput isKindOfClass:[PinAnnotationPresenter class]])
            {
                continue;
            }
            [strongOutput updateState:value];
        }
    }
}

@end
