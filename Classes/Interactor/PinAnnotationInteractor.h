//
//  PinAnnotationInteractor.h
//  codesample
//
//  Created by Sergey Emelyanov on 12/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PinAnnotationInteractorIO.h"

@interface PinAnnotationInteractor : NSObject <PinAnnotationInteractorInput>

@property (nonatomic, weak) id<NSObject>    output;
@property(nonatomic,strong) NSMutableArray  *outputList;

@end
