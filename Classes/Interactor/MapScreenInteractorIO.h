//
//  MapScreenInteractorIO.h
//  codesample
//
//  Created by Sergey Emelyanov on 05/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol MapScreenInteractorInput <NSObject>

- (void)requestData;
- (void)sendData;

@end

@protocol MapScreenInteractorOutput <NSObject>

- (void)updateCompanyList:(NSArray*)value;
- (void)sendError:(NSError*)error;

@end
