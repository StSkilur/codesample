//
//  main.m
//  codesample
//
//  Created by Sergey Emelyanov on 04/03/2017.
//  Copyright © 2017 Sergey Emelyanov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
